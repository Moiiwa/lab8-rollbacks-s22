import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=postgres user=postgres")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
select_players_inventory = "SELECT username, product, amount FROM Inventory WHERE username = %(username)s"
add_to_inv = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s)"
update_inv = "UPDATE Inventory SET amount = amount + %(amount)s WHERE username = %(username)s AND product = %(product)s"


def is_amount_overflow(cursor, obj):
    sum = 0
    for row in cursor:
        sum += row[2]

    return (sum + obj.get('amount')) > 100


def record_exists(cur, obj):
    for row in cur:
        if row[0] == obj.get('username') and row[1] == obj.get('product'):
            return True
    return False


def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")
            cur.execute(select_players_inventory, obj)
            if cur.rowcount == 0:
                cur.execute(add_to_inv, obj)
            else:
                if not is_amount_overflow(cur, obj):
                    cur.execute(select_players_inventory, obj)
                    if record_exists(cur, obj):
                        cur.execute(update_inv, obj)
                    else:
                        cur.execute(add_to_inv, obj)
                else:
                    raise Exception("Inventory is full")

            conn.commit()


buy_product('Bob', 'marshmello', 1)
